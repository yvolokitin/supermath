package ru.SuperMath;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class IndicatorsHelper extends InvisibleRun{
    private WebDriver driver = getDriver();
    public int tasks_remain;
    public int tasks_all;
    public int tasks_passed;
    public int tasks_failed;

    public IndicatorsHelper() {
        tasks_remain = Integer.parseInt(getDriver().findElement(By.id("tasks_remain")).getText());
        tasks_all = Integer.parseInt(getDriver().findElement(By.id("tasks_all")).getText());
        tasks_passed = Integer.parseInt(getDriver().findElement(By.id("tasks_passed")).getText());
        tasks_failed = Integer.parseInt(getDriver().findElement(By.id("tasks_failed")).getText());
    }
}
