package ru.SuperMath;

import junit.framework.TestCase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import java.util.Properties;
import java.io.*;


//named incorrect
public class VisibleRun {
    private WebDriver driver; // make privet and create getter
    private WebDriverWait driverWait; // make privet and create getter
    private MainPage mainPage;


    public WebDriver getDriver() {
        return this.driver;
    }

    public WebDriverWait getDriverWait() {
        return this.driverWait;
    }

    public MainPage getMainPage() {
        return this.mainPage;
    }

    @BeforeSuite
    public void setUpForSuit() {
        System.setProperty("webdriver.chrome.driver","C:/Users/ddzheber/Downloads/chromedriver_win32/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driverWait = new WebDriverWait(driver, 120);
    }

    @BeforeSuite
    public void setUp() {
        mainPage = new MainPage(getDriver(), getDriverWait());
        mainPage.pageOpen();
        String mainTitle = "SuperMath - Математика, обучение сложению, вычитанию, умножению и делению";
        String pageTitle = getDriver().getTitle();
        assert(pageTitle.equals(mainTitle));
    }

    @AfterSuite
    public void tearDown() {
        if (driver != null)
            driver.quit();
    }
}