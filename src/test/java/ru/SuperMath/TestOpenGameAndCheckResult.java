package ru.SuperMath;

import org.testng.annotations.Test;

public class TestOpenGameAndCheckResult extends InvisibleRun {


    private void openGameAndCheckResult(int gameId){
        MainPage mainPage = getMainPage();
        GameObjectImpl game1 = mainPage.gameOpen(gameId);
        boolean[] typeCheck = game1.checkGameResult();
        boolean isKeys = typeCheck[0];
        boolean isMoreLessButtons = typeCheck[1];
        if (isKeys){
            int[] resultWithKeys = game1.checkResultWithKeys();
            assert resultWithKeys[0] == resultWithKeys[1] : "Wrong expected result";
        } else if(isMoreLessButtons){
            String[] resultsWithMoreLessButtons = game1.checkResultWithMoreLessButtons();
            assert resultsWithMoreLessButtons[0].equals(resultsWithMoreLessButtons[1]) : "Wrong expected result";
        }

        game1.clickCloseGame();

    }
    @Test
    public void testOpenGame1AndCheckResult() {
        openGameAndCheckResult(1);
    }

    @Test
    public void testOpenGame2AndCheckResult() {
        openGameAndCheckResult(2);
    }

    @Test
    public void testOpenGame3AndCheckResult() {
        openGameAndCheckResult(3);
    }

    @Test
    public void testOpenGame4AndCheckResult() {
        openGameAndCheckResult(4);
    }

    @Test
    public void testOpenGame5AndCheckResult() {
        openGameAndCheckResult(5);
    }


}

