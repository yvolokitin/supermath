package ru.SuperMath;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.Properties;
import java.io.*;

public class GameObjectImpl extends GameObject{

    private By taskDiv;
    private By win_close;
    private By keys;
    private By indicators;
    private By moreLess;
    private By firstNumberWithKeys;
    private By firstNumberWithMoreLessButtons;
    private By secondNumberWithKeys;
    private By secondNumberWithMoreLessButtons;
    private By operation;
    private By operationResultWithKeys;
    private By operationResultWithMoreLessBattons;
    private By tasks_remain;
    private By tasks_all;
    private By tasks_passed;
    private By tasks_failed;
    private By button0;
    private By button1;
    private By button2;
    private By button3;
    private By button4;
    private By button5;
    private By button6;
    private By button7;
    private By button8;
    private By button9;
    private By buttonMore;
    private By buttonLess;
    private By buttonEquel;
    private By clock_for_time_out;
    private By clock_img;
    private By clock;
    private By keys_img;
    private By image_img;
    private By image;
    private By gameDiv;
    WebDriver driver;
    WebDriverWait driverWait;

 // объяви WebDriverWait  в одном месте.
    public GameObjectImpl(WebDriver driver, WebDriverWait driverWait) {
        this.driver = driver;
        this.driverWait = driverWait;
        FileInputStream file;
        Properties property = new Properties();

        try {
            file = new FileInputStream("src\\main\\resources\\config.properties");
            property.load(file);
            button0 = By.id(property.getProperty("button0"));
            button1 = By.id(property.getProperty("button1"));
            button2 = By.id(property.getProperty("button2"));
            button3 = By.id(property.getProperty("button3"));
            button4 = By.id(property.getProperty("button4"));
            button5 = By.id(property.getProperty("button5"));
            button6 = By.id(property.getProperty("button6"));
            button7 = By.id(property.getProperty("button7"));
            button8 = By.id(property.getProperty("button8"));
            button9 = By.id(property.getProperty("button9"));
            buttonMore = By.id(property.getProperty("buttonMore"));
            buttonLess = By.id(property.getProperty("buttonLess"));
            buttonEquel = By.id(property.getProperty("buttonEquel"));
            taskDiv = By.id(property.getProperty("taskDiv"));
            win_close = By.xpath(property.getProperty("win_close"));
            keys = By.id(property.getProperty("keys"));
            indicators = By.id(property.getProperty("indicators"));
            moreLess = By.id(property.getProperty("moreLess"));
            firstNumberWithKeys = By.id(property.getProperty("firstNumberWithKeys"));
            firstNumberWithMoreLessButtons = By.id(property.getProperty("firstNumberWithMoreLessButtons"));
            secondNumberWithKeys = By.id(property.getProperty("secondNumberWithKeys"));
            secondNumberWithMoreLessButtons = By.id(property.getProperty("secondNumberWithMoreLessButtons"));
            operation = By.id(property.getProperty("operation"));
            operationResultWithKeys = By.id(property.getProperty("operationResultWithKeys"));
            operationResultWithMoreLessBattons = By.id(property.getProperty("operationResultWithMoreLessBattons"));
            tasks_remain = By.id(property.getProperty("tasks_remain"));
            tasks_all = By.id(property.getProperty("tasks_all"));
            tasks_passed = By.id(property.getProperty("tasks_passed"));
            tasks_failed = By.id(property.getProperty("tasks_failed"));
            clock_for_time_out = By.id(property.getProperty("clock_for_time_out"));
            clock_img = By.id(property.getProperty("clock_img"));
            clock = By.id(property.getProperty("clock"));
            keys_img = By.id(property.getProperty("keys_img"));
            image_img = By.id(property.getProperty("image_img"));
            image = By.id(property.getProperty("image"));
            gameDiv = By.id(property.getProperty("gameDiv"));
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void clickCloseGame(){
        driver.findElement(win_close).click();
    }
    public boolean[] checkGameResult() {
        //(new WebDriverWait(driver, 180)).until(ExpectedConditions.visibilityOfElementLocated(taskDiv));
        boolean isKeys = false;
        boolean isMoreLessButtons = false;
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (driver.findElement(keys).isDisplayed()) {
            isKeys = true;
        } else if (driver.findElement(moreLess).isDisplayed()) {
            isMoreLessButtons = true;
        }
        boolean[] results = {isKeys, isMoreLessButtons};
        return results;
    }

    public int[] checkResultWithKeys() {
        String firstNumberVar = getElementContent(firstNumberWithKeys);
        String secondNumberVar = getElementContent(secondNumberWithKeys);
        String operationVar = getElementContent(operation);
        int actualResult = getResultWithKeys(firstNumberVar , secondNumberVar, operationVar);
        enterResult(actualResult + 1);
        String operationResultVar = getElementContent(operationResultWithKeys);
        int[] localResults = {Integer.parseInt(operationResultVar), actualResult};

        return localResults;
    }

    public String[] getPartsOfTask() {
        String[] result = {driver.findElement(firstNumberWithKeys).getText(),
                driver.findElement(secondNumberWithKeys).getText(),
                driver.findElement(operation).getText()};

        return result;
    }

    public int[] getIndicators(){

        int[] result = {(Integer.parseInt(getElementContent(tasks_passed))),
                (Integer.parseInt(getElementContent(tasks_failed))),
                (Integer.parseInt(getElementContent(tasks_remain))),
                (Integer.parseInt(getElementContent(tasks_all)))};
        return result;
    }

    public String getElementContent(By link){
        String result = driver.findElement(link).getText();
        return result;
    }

    public void enterResult(int result) {
        String strResult = String.valueOf(result);
        for(int i = 0; i < strResult.length(); i++){
            String subStr = strResult.substring(i, i + 1);
            pressButton(subStr);
        }
    }

    public void waitIndicatorsLoad() {
        //(new WebDriverWait(driver, 80)).until(ExpectedConditions.visibilityOfElementLocated(indicators));
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(indicators));
    }

    public void waitTaskBegin() {
        //(new WebDriverWait(driver, 80)).until(ExpectedConditions.visibilityOfElementLocated(taskDiv));
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(taskDiv));
    }

    public void watTimeOut(){
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(clock_for_time_out));
    }

    public void pressButton(String number) {
        By buttonLink = By.id("");
        switch (number.charAt(0)){
            case '0' :
                buttonLink = button0;
                break;
            case '1' :
                buttonLink = button1;
                break;
            case '2' :
                buttonLink = button2;
                break;
            case '3' :
                buttonLink = button3;
                break;
            case '4' :
                buttonLink = button4;
                break;
            case '5' :
                buttonLink = button5;
                break;
            case '6' :
                buttonLink = button6;
                break;
            case '7' :
                buttonLink = button7;
                break;
            case '8' :
                buttonLink = button8;
                break;
            case '9' :
                buttonLink = button9;
                break;
            case '<' :
                buttonLink = buttonLess;
                break;
            case '>' :
                buttonLink = buttonMore;
                break;
            case '=' :
                buttonLink = buttonEquel;
                break;
            default:
                System.out.println("invalid char:" + number.charAt(0));
                break;
        }
        driver.findElement(buttonLink).click();
    }

   /* public void enterResult(String strActualResult) {
        for (int i = 0; i < strActualResult.length(); i++) {
            String subStr = strActualResult.substring(i, i + 1);
            pressButton(subStr);
        }
    }*/

    public int getResultWithKeys(String firstNumber , String secondNumber, String operationVar) {
        int localResult;
        switch (operationVar.charAt(0)){
            case '+' :
                localResult = Integer.parseInt(firstNumber.trim()) + Integer.parseInt(secondNumber.trim());
                break;
            case '-' :
                localResult = Integer.parseInt(firstNumber.trim()) - Integer.parseInt(secondNumber.trim());
                break;
            case 'x' :
                localResult = Integer.parseInt(firstNumber.trim()) * Integer.parseInt(secondNumber.trim());
                break;
            case '/' :
                localResult = Integer.parseInt(firstNumber.trim()) / Integer.parseInt(secondNumber.trim());
                break;
            default:
                localResult = 0;
                System.out.println("incorrect operator");
        }
        return localResult;
    }

    public String[] checkResultWithMoreLessButtons() {
        String firstNumberVar = driver.findElement(firstNumberWithMoreLessButtons).getText();
        String secondNumberVar = driver.findElement(secondNumberWithMoreLessButtons).getText();
        int resultButtonId = getResultWithMoreLessButtons(Integer.parseInt(firstNumberVar), Integer.parseInt(secondNumberVar));
        String resultButton = getResultButtonById(resultButtonId);
        pressButton(getResultButtonById(resultButtonId + 1));
        String operationResultVar = driver.findElement(operationResultWithMoreLessBattons).getText();
        String[] localResults = {operationResultVar, resultButton};
        //assert operationResultVar.equals(resultButton) : "Wrong expected result";

        return localResults;
    }

    public String getResultButtonById(int buttonId) {
        String resultButton = "";
        switch (buttonId){
            case 1 :
                resultButton = "<";
                break;
            case 2 :
                resultButton = "=";
                break;
            case 3 :
                resultButton = ">";
                break;
            case 4 :
                resultButton = "<";
                break;
        }

        return resultButton;
    }

    public int getResultWithMoreLessButtons(int firstNumber, int secondNumber) {
        int resultButtonId;
        if (firstNumber < secondNumber){
            resultButtonId = 1;
        } else if(firstNumber > secondNumber){
            resultButtonId = 3;
        } else {
            resultButtonId = 2;
        }

        return resultButtonId;

    }

    public void timeOutTest(int summ){}//rename waitGameTimeOut
    public void timeOutTest(){}//rename waitGameTimeOut
    public void incorrectAnswersTest(int summ){}//rename enterIncorrectResult
    public void incorrectAnswersTest(){}
    public void correctAnswersTest(int breakpoint){}
    public void correctAnswersTest(){}
}
