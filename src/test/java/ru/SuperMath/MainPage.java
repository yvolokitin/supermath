package ru.SuperMath;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.Properties;
import java.io.*;

public class MainPage extends PageObject{

    By help_element;
    By about_element;
    By sign_in_element;
    By open_reg;
    By game1;
    By game2;
    By game3;
    By game4;
    By game5;
    By gameDiv;
    String mainLink;
    WebDriver driver;
    WebDriverWait driverWait;

    public MainPage(WebDriver driver, WebDriverWait driverWait)
    {
        this.driver = driver;
        this.driverWait = driverWait;
        FileInputStream file;
        Properties property = new Properties();
        try {
            file = new FileInputStream("src\\main\\resources\\config.properties");
            property.load(file);
            help_element = By.id(property.getProperty("help_element"));
            about_element = By.id(property.getProperty("about_element"));
            sign_in_element = By.id(property.getProperty("sign_in_element"));
            open_reg = By.id(property.getProperty("open_reg"));
            game1 = By.id(property.getProperty("game1"));
            game2 = By.id(property.getProperty("game2"));
            game3 = By.id(property.getProperty("game3"));
            game4 = By.id(property.getProperty("game4"));
            game5 = By.id(property.getProperty("game5"));
            gameDiv = By.id(property.getProperty("gameDiv"));
            mainLink = property.getProperty("mainLink");
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void pageOpen() {
        driver.get(mainLink);
    }

    public void pageClose() {
        if (driver != null)
            driver.quit();
    }

    public PageObject helpClick()
    {
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(help_element));
         driver.findElement(help_element).click();

        HelpPage help_page = new HelpPage(driver);
        return help_page;
    }

    public PageObject aboutClick()
    {
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(about_element));
        driver.findElement(about_element).click();
        AboutPage about_page = new AboutPage(driver);
        return about_page;
    }

    public PageObject signInClick()
    {
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(sign_in_element));
        driver.findElement(sign_in_element).click();
        SignInPage signInObj = new SignInPage(driver);
        return signInObj;
    }

    public void regOpen()
    {
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(open_reg));
        driver.findElement(open_reg).click();
    }

    public By getGameElementId(int gameId) {
        By result = By.className("");
        switch (gameId){
            case 1 :
                result = game1;
                break;
            case 2 :
                result = game2;
                break;
            case 3 :
                result = game3;
                break;
            case 4 :
                result = game4;
                break;
            case 5 :
                result = game5;
                break;
            default:
                break;
        }
        return result;
    }

    public GameObjectImpl gameOpen(int gameId) {
        By gameElementId = getGameElementId(gameId);
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(gameElementId));
        driver.findElement(gameElementId).click();
        GameObjectImpl GameObj = new GameObjectImpl(driver, driverWait);
        return GameObj;
    }


    public MainPage getMainPage() {
        MainPage mainPage = new MainPage(driver, driverWait);
        return mainPage;
    }
    

}
