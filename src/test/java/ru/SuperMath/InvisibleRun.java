package ru.SuperMath;

import junit.framework.TestCase;
import org.apache.xpath.operations.String;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

public class InvisibleRun  {
    private WebDriver driver; // make privet and create getter
    private MainPage mainPage;
    private WebDriverWait driverWait;

    public WebDriver getDriver() {
        return this.driver;
    }

    public MainPage getMainPage() {
        return this.mainPage;
    }

    public WebDriverWait getDriverWait() {
        return this.driverWait;
    }

    @BeforeSuite
    public void setUpForClass() {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\ddzheber\\WebRdivers\\chromedriver_win32\\chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("--window-size=1920,1080");
        driver = new ChromeDriver(chromeOptions);
        driverWait = new WebDriverWait(driver, 120);
    }

    @BeforeSuite
    public void setUpFotTest() {
        System.out.println("miay");
        mainPage = new MainPage(getDriver(), getDriverWait());
        mainPage.pageOpen();
        java.lang.String pageTitle = getDriver().getTitle();
        assert(pageTitle.equals("SuperMath - Математика, обучение сложению, вычитанию, умножению и делению"));// create method for getter mainPage and test it
    }

    @AfterSuite
    public void tearDown() {
        if (driver != null)
            driver.quit();
    }
}
