﻿CREATE TABLE `users` (
    `ID` bigint(20) NOT NULL auto_increment,
    `NAME` varchar(64) COLLATE `utf8_general_ci` NOT NULL,
    `SURNAME` varchar(64) COLLATE `utf8_general_ci` NOT NULL,
    `EMAIL` varchar(255) COLLATE `utf8_general_ci` NOT NULL,
    `PASSWORD` varchar(35) COLLATE `utf8_general_ci` NOT NULL,
    `PSWD_HASH` varchar(32) NOT NULL,
    `USERGROUP` enum('GUEST', 'USER', 'PAID', 'ADMIN', 'FAKE') NOT NULL DEFAULT 'GUEST',
    `UUID` varchar(32) COLLATE `utf8_general_ci` NOT NULL,
    `IP` varchar(32) COLLATE `utf8_general_ci` NOT NULL,
    `CREATION_DATE` datetime NOT NULL,
    `CURRENT_PROGRAM` varchar(128) COLLATE `utf8_general_ci` DEFAULT '',
    `STARS` bigint(20) NOT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `EMAIL_IX` (`EMAIL`)
) ENGINE=MyISAM DEFAULT CHARACTER SET `utf8` COLLATE=utf8_general_ci AUTO_INCREMENT=1;


ALTER TABLE users MODIFY `USERGROUP` enum('GUEST', 'USER', 'PAID', 'ADMIN', 'FAKE') NOT NULL DEFAULT 'GUEST';

INSERT INTO users (`NAME`, `SURNAME`, `EMAIL`, `PASSWORD`, `PSWD_HASH`, `USERGROUP`, `UUID`, `IP`, `CREATION_DATE`, `STARS`)  VALUES ('Anna', 'Koltcsman', 'aankotc@gamil.com', '123456789', 'fake', 'FAKE', 'fake', '10.10.10.10', '2017-02-03 23:36:01', '0');
INSERT INTO users (`NAME`, `SURNAME`, `EMAIL`, `PASSWORD`, `PSWD_HASH`, `USERGROUP`, `UUID`, `IP`, `CREATION_DATE`, `STARS`)  VALUES ('Max', 'Selikov', 'sqqwy@gamil.com', '123456789', 'fake', 'FAKE', 'fake', '10.10.10.10', '2017-02-03 23:36:01', '0');
INSERT INTO users (`NAME`, `SURNAME`, `EMAIL`, `PASSWORD`, `PSWD_HASH`, `USERGROUP`, `UUID`, `IP`, `CREATION_DATE`, `STARS`)  VALUES ('Sajash', 'Subramanjakha', 'shgas@gamil.com', '123456789', 'fake', 'FAKE', 'fake', '10.10.10.10', '2017-02-03 23:36:01', '0');
INSERT INTO users (`NAME`, `SURNAME`, `EMAIL`, `PASSWORD`, `PSWD_HASH`, `USERGROUP`, `UUID`, `IP`, `CREATION_DATE`, `STARS`)  VALUES ('TJ', 'Communol', 'tjcom@gamil.com', '123456789', 'fake', 'FAKE', 'fake', '10.10.10.10', '2017-02-03 23:36:01', '0');

DELETE FROM `users` WHERE `ID` = 8;
