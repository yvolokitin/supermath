# mysql -usupermath -pKiaForte2015! supermath_db;

# SET CHARACTER SET utf8;

# JavaScript Obfuscator https://habrahabr.ru/post/112530/
# from http://discogscounter.getfreehosting.co.uk/js-noalnum_com.php?i=1

# CREATE DATABASE supermath_db;
CREATE DATABASE `supermath_db` CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

# Table structure `users`
# removed elements:
# `MIDDLE_NAME` varchar(35) collate utf8_bin default NULL,
# Пока неизвестно как хранить русские буквы, БД записывает их коряво

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
    `ID` bigint(20) NOT NULL auto_increment,
    `NAME` varchar(64) COLLATE `utf8_general_ci` NOT NULL,
    `SURNAME` varchar(64) COLLATE `utf8_general_ci` NOT NULL,
    `EMAIL` varchar(255) COLLATE `utf8_general_ci` NOT NULL,
    `PASSWORD` varchar(35) COLLATE `utf8_general_ci` NOT NULL,
    `PSWD_HASH` varchar(32) NOT NULL,
    `USERGROUP` enum('GUEST', 'USER', 'PAID', 'ADMIN', 'FAKE') NOT NULL DEFAULT 'GUEST',
    `UUID` varchar(32) COLLATE `utf8_general_ci` NOT NULL,
    `IP` varchar(32) COLLATE `utf8_general_ci` NOT NULL,
    `CREATION_DATE` datetime NOT NULL,
    `PROGRAM` varchar(128) COLLATE `utf8_general_ci` DEFAULT '',
    `BELT` varchar(128) COLLATE `utf8_general_ci` DEFAULT '',
    `STARS` bigint(20) NOT NULL,
    `SOLVED` varchar(1024) COLLATE `utf8_general_ci` DEFAULT '',
    `AVATAR` varchar(128) COLLATE `utf8_general_ci` DEFAULT 'avatars/user_logo.jpg';
    `SET_HERO` varchar(5) COLLATE `utf8_general_ci` DEFAULT 'true';
    `SET_KBRD` varchar(5) COLLATE `utf8_general_ci` DEFAULT 'true';
    `SET_TIME` varchar(5) COLLATE `utf8_general_ci` DEFAULT 'true';
    `SET_LNGD` varchar(5) COLLATE `utf8_general_ci` DEFAULT 'ru';
    PRIMARY KEY (`ID`),
    UNIQUE KEY `EMAIL_IX` (`EMAIL`)
) ENGINE=MyISAM DEFAULT CHARACTER SET `utf8` COLLATE=utf8_general_ci AUTO_INCREMENT=1;


ALTER TABLE users ADD `SOLVED` varchar(1024) COLLATE `utf8_general_ci` DEFAULT '';
ALTER TABLE users MODIFY `SOLVED` varchar(1024) COLLATE `utf8_general_ci` DEFAULT '';
ALTER TABLE users ADD `AVATAR` varchar(128) COLLATE `utf8_general_ci` DEFAULT 'avatars/user_logo.jpg';

ALTER TABLE users ADD `SET_HERO` varchar(5) COLLATE `utf8_general_ci` DEFAULT 'true';
ALTER TABLE users ADD `SET_KBRD` varchar(5) COLLATE `utf8_general_ci` DEFAULT 'true';
ALTER TABLE users ADD `SET_TIME` varchar(5) COLLATE `utf8_general_ci` DEFAULT 'true';
ALTER TABLE users ADD `SET_LNGD` varchar(5) COLLATE `utf8_general_ci` DEFAULT 'ru';

Alter table bs_merge_queue modify description varchar(8192);

DROP TABLE IF EXISTS `registration_failed`;
CREATE TABLE `registration_failed` (
    `ID` bigint(20) NOT NULL auto_increment,
    `EMAIL` varchar(255) collate utf8_general_ci NOT NULL,
    PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARACTER SET `utf8` COLLATE=utf8_general_ci AUTO_INCREMENT=1;


# Table structure `programs`
# каждая программа - отдельная игра, gamer хранит ссылку на текущую назначенную игру
# NAME - название программы, например: Сложение чисел до 10 и т.д.
# TYPE - тип программы USER, PAID, VIP -> соответствует типу пользователя. USER бесплатная, PAID - за деньги, VIP - пока не используется
# ASSET - описание операций в программе и порядрок чисел
# DESCRIPTION - детальное описание программы
# WHITEBOARD_ID_NAME указывает какой графический элемент DIV использовать для отрисовки
DROP TABLE IF EXISTS `programs`;
CREATE TABLE `programs` (
    `ID` bigint(20) NOT NULL auto_increment,
    `NAME` varchar(255) NOT NULL,
    `TYPE` enum('USER', 'PAID', 'VIP') NOT NULL DEFAULT 'USER',
    `DESCRIPTION` varchar(255) collate utf8_bin NOT NULL default '',
    `WHITEBOARD_ID_NAME` varchar(128) NOT NULL DEFAULT 'whiteboard_math_operations_game_id',
    PRIMARY KEY (`ID`),
    UNIQUE KEY `NAME_IX` (`NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;

INSERT INTO programs (`NAME`, `DESCRIPTION`) VALUES ('Сложение чисел от 0 до 10', '+:10');
INSERT INTO programs (`NAME`, `DESCRIPTION`) VALUES ('Вычитание чисел от 0 до 10', '-:10');
INSERT INTO programs (`NAME`, `DESCRIPTION`) VALUES ('Сложение и Вычитание чисел от 0 до 10', '+-:10');
INSERT INTO programs (`NAME`, `DESCRIPTION`) VALUES ('Умножение чисел от 0 до 10', '*:10');
INSERT INTO programs (`NAME`, `DESCRIPTION`) VALUES ('Деление чисел от 0 до 10', '/:10');
INSERT INTO programs (`NAME`, `DESCRIPTION`) VALUES ('Сложение чисел до 100', '+:100');
INSERT INTO programs (`NAME`, `DESCRIPTION`) VALUES ('Вычитание чисел до 100', '-:100');
INSERT INTO programs (`NAME`, `DESCRIPTION`) VALUES ('Сложение и Вычитание чисел до 100', '+-:100');
INSERT INTO programs (`NAME`, `DESCRIPTION`) VALUES ('Сложение чисел до 1000', '+:1000');
INSERT INTO programs (`NAME`, `DESCRIPTION`) VALUES ('Вычитание чисел до 1000', '-:1000');
INSERT INTO programs (`NAME`, `DESCRIPTION`) VALUES ('Сложение и Вычитание чисел до 1000', '+-:1000');
INSERT INTO programs (`NAME`, `DESCRIPTION`) VALUES ('Сложение Вычитание Деление и Умножение', '+-*/:100');
INSERT INTO programs (`NAME`, `DESCRIPTION`) VALUES ('Сложение и Вычитание Отрицательных чисел', '+-:-100');
































DROP TABLE IF EXISTS `gamers`;
CREATE TABLE `gamers` (
    `ID` bigint(20) NOT NULL auto_increment,
    `USER_ID` bigint(20) default NULL,
    `NAME` varchar(255) collate utf8_bin NOT NULL,
    `BIRTHDAY` datetime default NULL,
    `PROGRAM_ID` bigint(20) default NULL,
    PRIMARY KEY (`ID`),
    KEY `FK_GAMER_USER_ID` (`USER_ID`),
    CONSTRAINT `FK_GAMER_USER_ID` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`),
    KEY `FK_GAMER_PROGRAM_ID` (`PROGRAM_ID`),
    CONSTRAINT `FK_GAMER_PROGRAM_ID` FOREIGN KEY (`PROGRAM_ID`) REFERENCES `programs` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;
