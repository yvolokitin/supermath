﻿import ftplib
import sys, os

import urllib2, json

def ftp_upload(source_file_path):
    try:
        session = ftplib.FTP('supermath.ru', 'z1usen38', 'Dodge2015!')
        if os.path.isfile(source_file_path):
            file = open(source_file_path,'rb')
            session.storbinary('STOR sm.tgz', file)
            file.close()
        else:
            print "FILE ERROR: Source File does not exist"
            sys.exit(os.EX_SOFTWARE)

        session.quit()

    except ftplib.all_errors as e:
        print "FTP ERROR: Could not open FTP connection %s" % e
        sys.exit(os.EX_SOFTWARE)

def call_deployer(operation, path, archive_name):
    data = json.dumps({'operation': operation, 'path': path, 'name': archive_name})
    headers = {'Content-Type': 'application/json', 'Content-Length': len(data)}
    request = urllib2.Request('http://supermath.ru/php/deployer.php', data, headers)
    response = urllib2.urlopen(request)
    message = response.read()
    response.close()
    print message

if __name__ == "__main__":
    ftp_upload('/builds/yvolokitin/supermath/sm.tgz')
    call_deployer('deploy', '/home/z1usen38/public_html/test/', 'sm.tgz')

    # Exit code 0, ALL OK
    sys.exit(os.EX_OK)
