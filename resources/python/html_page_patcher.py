﻿import os, sys, tempfile

IN = '<script type="text/javascript" src="js/consts.js"></script><script type="text/javascript" src="js/scripts.js"></script><script type="text/javascript" src="js/tools.js"></script>'
TO = '<script type="text/javascript" src="js/jquery.js">'

def find_and_replace(file_name='/builds/yvolokitin/supermath/index.html'):
    fd, temp_file = tempfile.mkstemp()
    with open(file_name) as fd1, open(temp_file, 'w') as fd2:
        for line in fd1:
            line = line.replace(IN, TO)
            fd2.write(line)

    os.rename(temp_file, file_name)
    fd.close()

    fd1.close()
    fd2.close()

if __name__ == "__main__":
   find_and_replace('/builds/yvolokitin/supermath/index.html')
